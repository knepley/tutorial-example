Steps.pdf : 
	TEXINPUTS=${TEXINPUTS}:./tex BIBINPUTS=${BIBINPUTS}:${PETSC_DIR}/src/docs/tex latexmk -pdf Steps.tex

clean:
	rm Steps.aux Steps.log Steps.out

.PHONY : Steps.pdf
